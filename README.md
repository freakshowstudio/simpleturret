About
=====

A simple example for Unity showing how to create a turret that can track a target.

The turret is made of two parts, a base and a gun barrel, each of which track in separate axis. The base
tracks rotation around the Y axis, while the barrel tracks the target up and down.


License
=======

This software is released under the [UNLICENSE](http://unlicense.org/)
license, see the file UNLICENSE for more information.

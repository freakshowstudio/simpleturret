﻿using UnityEngine;
using System.Collections;

public class TurretBehaviour : MonoBehaviour
{
    #region Inspector Variables
    [Header("Turret Target")]

    [Tooltip("The Target that the Turret should be tracking")]
    [SerializeField] private Transform _target;


    [Header("Turret Elements")]

    [Tooltip("The base of the Turret, rotates around the Y axis")]
    [SerializeField] private Transform _base;

    [Tooltip("The gun barrel of the Turret, rotates up and down")]
    [SerializeField] private Transform _gun;


    [Header("Turret Properties")]

    [Tooltip("How fast the base should rotate towards the target")]
    [SerializeField] private float _baseRotationSpeed = 100f;

    [Tooltip("How fast the gun barrel should rotate towards the target")]
    [SerializeField] private float _gunRotationSpeed = 100f;

    [Header("User Interface")]

    [Tooltip("Position and size of Randomize Target Position button")]
    [SerializeField] private Rect _randomizeTargetPositionButtonRect =
        new Rect(10f, 10f, 250f, 20f);

    [Tooltip("Maximum distance from turret to randomized position of target")]
    [SerializeField] private float _randomizeTargetPositionMaxRadius = 5f;
    #endregion // Inspector Variables

    #region Unity Methods
    void Update()
    {
        // Calculate the rotation for the base
        Vector3 baseDirection = _target.position - transform.position;

        Vector3 baseDirectionInPlane =
            Vector3.ProjectOnPlane(baseDirection, transform.up);

        if (!Mathf.Approximately(baseDirectionInPlane.sqrMagnitude, 0f))
        {
            Quaternion baseRotation =
                Quaternion.LookRotation(baseDirectionInPlane);

            _base.rotation =
            Quaternion.RotateTowards(
                _base.rotation,
                baseRotation,
                _baseRotationSpeed * Time.deltaTime);
        }


        // Calculate the rotation for the barrel
        Vector3 gunDirection = _target.position - _gun.position;

        Vector3 gunDirectionInPlane =
            Vector3.ProjectOnPlane(gunDirection, transform.up);

        float gunTargetAngle =
            Mathf.Atan2(
                gunDirection.y,
                gunDirectionInPlane.magnitude) * -Mathf.Rad2Deg;

        Quaternion gunRotation =
            Quaternion.Euler(gunTargetAngle, 0f, 0f);

        _gun.localRotation =
            Quaternion.RotateTowards(
                _gun.localRotation,
                gunRotation,
                _gunRotationSpeed * Time.deltaTime);
    }

    void OnGUI()
    {
        bool randomizeTargetPosition =
            GUI.Button(
                _randomizeTargetPositionButtonRect,
                "Randomize Target Position");

        if (randomizeTargetPosition)
        {
            Vector3 newTargetPosition =
                UnityEngine.Random.insideUnitSphere *
                _randomizeTargetPositionMaxRadius;

            if (newTargetPosition.y < 0f)
            {
                newTargetPosition =
                    Vector3.Scale(
                        newTargetPosition,
                        new Vector3(0f, -1f, 0f));
            }

            _target.position = newTargetPosition;
        }
    }
    #endregion // Unity Methods
}
